import {
    FETCH_COUNTER_SUCCESS, FETCH_COUNTER_ERROR,
    FETCH_INCREMENTION_REQUEST, FETCH_INCREMENTION_SUCCESS, FETCH_INCREMENTION_ERROR
} from "./actions";

const initialState = {
    loading: false,
    counter: 0
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COUNTER_SUCCESS:
            return {counter: action.counter};
        case FETCH_COUNTER_ERROR:
            return {counter: action.error};
        case FETCH_INCREMENTION_REQUEST:
            return {...state, loading: true};
        case FETCH_INCREMENTION_SUCCESS:
            return {counter: action.counter};
        case FETCH_INCREMENTION_ERROR:
            return {counter: action.error};
        default:
            return state;
    }
};


export default reducer;