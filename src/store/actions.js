import axios from '../axios-counter';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';
export const FETCH_INCREMENTION_REQUEST = 'FETCH_INCREMENTION_REQUEST';
export const FETCH_INCREMENTION_SUCCESS = 'FETCH_INCREMENTION_SUCCESS';
export const FETCH_INCREMENTION_ERROR = 'FETCH_INCREMENTION_ERROR';

export const fetchIncrementionRequest = () =>{return { type: FETCH_INCREMENTION_REQUEST}};
export const fetchIncrementionSuccess = (counter) =>({ type: FETCH_INCREMENTION_SUCCESS, counter});
export const fetchIncrementionError = () =>{return { type: FETCH_INCREMENTION_ERROR}};

export const fetchCounterRequest = () => {return { type: FETCH_COUNTER_REQUEST };};
export const fetchCounterSuccess = (counter) => {return { type: FETCH_COUNTER_SUCCESS, counter};};
export const fetchCounterError = (error) => {return { type: FETCH_COUNTER_ERROR, error};};


export const fetchCounter = () => {
    return(dispatch) => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(
            response => {
            dispatch(fetchCounterSuccess(response.data.counter));
        }, error => {
            dispatch(fetchCounterError(error))
        })
    }
};

export const fetchIncremention = (amount) => {
    return(dispatch, getState) => {
        dispatch(fetchIncrementionRequest());
        const counter = getState().counter + amount;
        axios.put('/counter.json', {counter}).then(
            response => {
                dispatch(fetchIncrementionSuccess(response.data.counter));
            }, error => {
                dispatch(fetchIncrementionError(error));
            }
        )
    }
};
